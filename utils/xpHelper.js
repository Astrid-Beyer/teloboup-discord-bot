const { User, Guild, CommandInteraction, EmbedBuilder } = require("discord.js");
const Level = require("../models/level");
const Leveling = require("../models/leveling");
const calculateLvlXP = require("./calculateLvlXP");
const { addToBal } = require("./currencyHelper");

/**
 * @description Vérifie si l'on changer de niveau après un changement d'XP
 * @param {Level} level - Objet contenant les infos du niveau
 * @returns {boolean} Indique si le niveau a changé
 */
const checkForLevelChange = (level) => {
  let hasLevelChanged = false;
  // Pour l'augmentation de niveau
  while (level.xp >= calculateLvlXP(level.level)) {
    level.xp -= calculateLvlXP(level.level);
    level.level += 1;
    hasLevelChanged = true;
  }

  // Pour la régression de niveau
  while (level.xp < 0) {
    level.level -= 1;
    level.xp = calculateLvlXP(level.level) + level.xp;
    hasLevelChanged = true;
  }

  return hasLevelChanged;
};
/**
 * @description Éxécute les actions à faire lors d'une augmentation de niveau
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel on veut faire les actions
 * @param {Level} level - Objet contenant les infos du niveau
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const doLevelUpActions = async (guild, user, level, interaction = null) => {
  const member = await guild.members.fetch(user.id);

  // Check des paliers pour les rôles

  let newRole = null;
  // On récupère tous les niveaux du serveur par ordre croissant
  const fetchedLevels = await Leveling.find({
    guildId: guild.id,
  }).sort({ level: 1 });

  let levelIndex = fetchedLevels.findIndex(
    (fetchedLevel) => fetchedLevel.level > level.level
  );
  if (levelIndex < 0) levelIndex = fetchedLevels.length;
  levelIndex--;

  if (!member.roles.cache.has(fetchedLevels[levelIndex].role)) {
    // Si le nouveau niveau est un palier
    // Le palier est à l'index levelIndex dans le tableau fetchedLevels et on peut y accéder avec fetchedLevels[levelIndex]
    for (let i = 0; i < levelIndex; i++) {
      member.roles.remove(fetchedLevels[i].role); // On retire le role des palier précédent à l'utilisateur
    }
    member.roles.add(fetchedLevels[levelIndex].role); // On ajoute le rôle du palier à l'utilisateur

    addToBal(guild, user, 1, interaction);
    newRole = await guild.roles.fetch(fetchedLevels[levelIndex].role);
  }

  // Message à afficher lors de la monté de niveau

  const channelId = "793651712341704775"; // channel bots
  const channel = await guild.channels.fetch(channelId);

  const newEmbed = new EmbedBuilder()
    .setColor(newRole ? newRole.color : 0xf3dbff)
    .setDescription(
      `**✧ niveau supérieur◞˚ₓ ☆**\n
			＼(٥⁀▽⁀ )／ Félicitations ${member} ! Tu passes au niveau ${
        level.level
      }. Continue comme ça !
			${
        newRole
          ? `\n\n ***Oh!*** Tu obtiens un nouveau rôle :\n> ${newRole.name}`
          : ""
      }`
    )
    .setThumbnail(user.displayAvatarURL());

  channel.send(user.toString());
  channel.send({ embeds: [newEmbed] });

  if (interaction?.isRepliable())
    interaction.followUp(`${user.displayName} passe au niveau ${level.level}.`);
};

/**
 * @description Éxécute les actions à faire lors d'une régression de niveau
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel on veut faire les actions
 * @param {Level} level - Objet contenant les infos du niveau
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const doLevelDownActions = async (guild, user, level, interaction = null) => {
  const member = await guild.members.fetch(user.id);

  // On récupère tous les niveaux du serveur par ordre croissant
  const fetchedLevels = await Leveling.find({
    guildId: guild.id,
  }).sort({ level: 1 });

  let levelIndex = fetchedLevels.findIndex(
    (fetchedLevel) => fetchedLevel.level > level.level
  );
  if (levelIndex < 0) levelIndex = fetchedLevels.length;
  levelIndex--;

  if (!member.roles.cache.has(fetchedLevels[levelIndex].role)) {
    // Si le nouveau niveau est un palier
    // Le palier est à l'index levelIndex dans le tableau fetchedLevels et on peut y accéder avec fetchedLevels[levelIndex]
    for (let i = levelIndex + 1; i < fetchedLevels.length; i++) {
      member.roles.remove(fetchedLevels[i].role); // On retire le role des palier suivant à l'utilisateur
    }
    member.roles.add(fetchedLevels[levelIndex].role); // On ajoute le rôle du palier à l'utilisateur

    newRole = await guild.roles.fetch(fetchedLevels[levelIndex].role);
  }
  if (interaction?.isRepliable())
    interaction.followUp(`${user.displayName} passe au niveau ${level.level}.`);
};

/**
 * @description Récupère les infos du niveau d'un utilisateur en BD ou le défaut si il n'est pas encore enregistré
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel on veut récupérer les infos
 */
const getSavedOrNewUserLevel = async (guild, user) => {
  const level = await Level.findOne({
    userId: user.id,
    guildId: guild.id,
  });

  // Si le niveau n'est pas trouvé on prend le défaut.
  return (
    level ??
    new Level({
      userId: user.id,
      guildId: guild.id,
    })
  );
};

/**
 * @description Ajoute de l'XP à un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel ajouter de l'XP
 * @param {number} amout - Nombre d'XP à ajouter
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const addXP = async (guild, user, amount, interaction = null) => {
  const level = await getSavedOrNewUserLevel(guild, user);
  level.xp += amount;
  if (checkForLevelChange(level))
    doLevelUpActions(guild, user, level, interaction);
  if (interaction?.isRepliable())
    interaction.followUp(
      `${user.displayName} a dorénavant ${level.xp} points d'XP, et est niveau ${level.level}.`
    );
  await level.save();
};

/**
 * @description Retire de l'XP à un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel retirer de l'XP
 * @param {number} amout - Nombre d'XP à retirer
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const removeXP = async (guild, user, amount, interaction = null) => {
  const level = await getSavedOrNewUserLevel(guild, user);
  level.xp -= amount;
  if (checkForLevelChange(level))
    doLevelDownActions(guild, user, level, interaction);
  if (interaction?.isRepliable())
    interaction.followUp(
      `${user.displayName} a dorénavant ${level.xp} points d'XP, et est niveau ${level.level}.`
    );
  await level.save();
};

/**
 * @description Défini le niveau d'XP d'un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel définir l'XP
 * @param {number} amout - Nombre d'XP
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const setXP = async (guild, user, amount, interaction = null) => {
  const level = await getSavedOrNewUserLevel(guild, user);
  level.xp = amount;
  const oldLevel = level.level;
  if (checkForLevelChange(level)) {
    const levelDiff = Math.abs(oldLevel - level.level);
    if (levelDiff > 0) doLevelUpActions(guild, user, level, interaction);
    else if (levelDiff < 0) doLevelDownActions(guild, user, level, interaction);
  }
  if (interaction?.isRepliable())
    interaction.followUp(
      `${user.displayName} a dorénavant ${level.xp} points d'XP.`
    );
  await level.save();
};

/**
 * @description Ajoute des niveaux à un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel ajouter des niveaux
 * @param {number} [amout=1] - Nombre de niveau à ajouter, 1 par défaut
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const addLevel = async (guild, user, amount = 1, interaction = null) => {
  const level = await getSavedOrNewUserLevel(guild, user);
  level.level += amount;
  level.xp = 0;
  doLevelUpActions(guild, user, level, interaction);
  await level.save();
};

/**
 * @description Retire des niveaux à un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel retirer des niveaux
 * @param {number} [amout=1] - Nombre de niveau à retirer, 1 par défaut
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const removeLevel = async (guild, user, amount = 1, interaction = null) => {
  const level = await getSavedOrNewUserLevel(guild, user);
  level.level -= amount;
  level.xp = 0;
  doLevelDownActions(guild, user, level, interaction);
  await level.save();
};

/**
 * @description Défini le niveau d'un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel définir le niveau
 * @param {number} amout - Niveau
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const setLevel = async (guild, user, amount, interaction = null) => {
  const level = await getSavedOrNewUserLevel(guild, user);
  const levelDiff = Math.abs(level.level - amount);
  level.level = amount;
  level.xp = 0;
  if (levelDiff > 0) doLevelUpActions(guild, user, level, interaction);
  else if (levelDiff < 0) doLevelDownActions(guild, user, level, interaction);
  await level.save();
};

module.exports = {
  getSavedOrNewUserLevel,
  addXP,
  removeXP,
  setXP,
  addLevel,
  removeLevel,
  setLevel,
};
