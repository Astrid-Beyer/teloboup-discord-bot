const fs = require("node:fs");

const getAllFiles = (dirPath, arrayOfFiles = []) => {
  files = fs.readdirSync(dirPath);
  files.forEach((file) => {
    const filePath = `${dirPath}/${file}`;
    if (fs.statSync(filePath).isDirectory())
      arrayOfFiles = getAllFiles(filePath, arrayOfFiles); // Dossier
    else arrayOfFiles.push(filePath); // Fichier
  });
  return arrayOfFiles;
};

module.exports = getAllFiles;
