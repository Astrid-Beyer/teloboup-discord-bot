const { User, Guild, CommandInteraction } = require("discord.js");
const Currency = require("../models/currency");

/**
 * @description Récupère les infos du solde d'un utilisateur en BD ou le défaut si il n'est pas encore enregistré
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur pour lequel on veut récupérer les infos
 */
const getSavedOrNewUserCurrency = async (guild, user) => {
  const currency = await Currency.findOne({
    userId: user.id,
    guildId: guild.id,
  });

  // Si l'utilisateur n'a pas encore de solde, on lui en créé un nouveau avec la valeur par défaut
  return (
    currency ??
    new Currency({
      userId: user.id,
      guildId: guild.id,
    })
  );
};

/**
 * @description Fixe le solde d'un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur qui va recevoir de la monnaie
 * @param {number} amount - Somme à fixer à l'utilisateur
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const setBal = async (guild, user, amount, interaction = null) => {
  const currency = await getSavedOrNewUserCurrency(guild, user);
  if (amount < 0) {
    interaction.followUp("⚠️ La balance ne peut pas être négative.");
    return;
  }
  currency.balance = amount;
  if (interaction?.isRepliable())
    interaction.followUp(
      `${user.displayName} a dorénavant un solde à ${currency.balance} ${getBalanceEmoji}.`
    );

  await currency.save();
};

/**
 * @description Augmente le solde d'un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur qui va recevoir de la monnaie
 * @param {number} amount - Somme à ajouter à l'utilisateur
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const addToBal = async (guild, user, amount, interaction = null) => {
  const currency = await getSavedOrNewUserCurrency(guild, user);
  currency.balance += amount;
  if (interaction?.isRepliable())
    interaction.followUp(
      `${user.displayName} reçoit ${amount} ${getBalanceEmoji}. Son solde est à ${currency.balance} ${getBalanceEmoji}`
    );
  await currency.save();
};

/**
 * @description Réduit le solde d'un utilisateur
 * @param {Guild} guild - Serveur dans lequel opérer
 * @param {User} user - Utilisateur qui va recevoir de la monnaie
 * @param {number} amount - Somme à retirer de l'utilisateur
 * @param {CommandInteraction} [interaction = null] - Interaction qui a déclenché la fonction. Si != null servira a fournir une réponse.
 */
const removeFromBal = async (guild, user, amount, interaction = null) => {
  const currency = await getSavedOrNewUserCurrency(guild, user);
  if (currency.balance - amount < 0) {
    interaction.followUp("⚠️ La balance ne peut pas être négative.");
    return;
  }
  currency.balance -= amount;
  if (interaction?.isRepliable())
    interaction.followUp(
      `${user.displayName} perd ${amount} ${getBalanceEmoji}. Son solde est à ${currency.balance} ${getBalanceEmoji}`
    );
  await currency.save();
};

/**
 * @description Emoji ID pour la balance du serveur
 */
const getBalanceEmoji = "<:spray_graff:1146017904836485241>";

module.exports = {
  setBal,
  addToBal,
  removeFromBal,
  getSavedOrNewUserCurrency,
  getBalanceEmoji,
};
