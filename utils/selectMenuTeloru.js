const fs = require("node:fs");
const {
  StringSelectMenuOptionBuilder,
  StringSelectMenuBuilder,
  ActionRowBuilder,
} = require("discord.js");

const Asset = require("../models/asset");
const userAsset = require("../models/userAsset");

const selectMenu = async (content) => {
  let userId = content.split(":")[0];
  let contents = content.split(",");
  let size = contents.length;

  const userAssets = await userAsset.find({ userId });
  let contentIndex = content.split(":")[1];
  contentIndex = contentIndex.split(",");
  let index = parseInt(contentIndex[size - 1].substring(0, 2)) + 1;
  if (index == 0) {
    index++;
  }

  assets = await Asset.find({
    assetId: { $regex: "^" + index },
  });

  while (assets.length == 0) {
    assets = await Asset.find({
      assetId: { $regex: "^" + index },
    });
    if (assets.length != 0) {
      break;
    }
    index++;
    if (index == 0) {
      index++;
    }
    if (index == 10) {
      break;
    }
  }

  let placeholder = "NaN";

  if (index == -1) {
    placeholder = "Choisis une coiffure";
  }
  if (index == 1) {
    placeholder = "Choisis une bouche";
  }
  if (index == 2) {
    placeholder = "Choisis un vetement";
  }
  if (index == 3) {
    placeholder = "Choisis un couvre-chef";
  }
  if (index == 4) {
    placeholder = "Choisis un sticker";
  }

  let itemCount = 0;

  let StringSelectMenuArray = [];

  assets.forEach((asset) => {
    let rarityEmote = "1147137357607350312";
    if (
      userAssets.find((userAsset) => userAsset.assetId == asset.assetId) ||
      asset.assetRarity == 0
    ) {
      if (asset.assetRarity == 2) {
        rarityEmote = "1147137261918507168"; // epic
      } else if (asset.assetRarity == 3) {
        rarityEmote = "1147135955183095808"; // legendary
      }
      itemCount++;
      StringSelectMenuArray.push(
        new StringSelectMenuOptionBuilder()
          .setLabel(asset.assetName)
          .setValue(content + "," + asset.assetId)
          .setEmoji(rarityEmote)
      );
    }
  });

  const selectMenu = new StringSelectMenuBuilder()
    .setCustomId("teloru" + index)
    .setPlaceholder(placeholder);

  if (itemCount > 0) {
    selectMenu.addOptions(
      StringSelectMenuArray.map((StringSelectMenuOptionBuilder) => {
        return StringSelectMenuOptionBuilder;
      })
    );
  }
  if (index == 3 || index == 4 || (itemCount == 0 && index != 2)) {
    selectMenu.addOptions(
      new StringSelectMenuOptionBuilder()
        .setLabel("Aucun")
        .setValue(content + "," + index + "_NaN")
    );
  }

  row = new ActionRowBuilder().addComponents(selectMenu);
  return row;
};

module.exports = selectMenu;
