const fs = require("node:fs");

const Asset = require("../models/asset");

const imageGenerator = async (content) => {
  content = content + ",0_base";
  let contents = content.split(":")[1];
  contents = contents.split(",");
  const assets = await Asset.find({
    assetId: { $in: contents },
  });

  let min = 10000;
  let max = -10000;
  assets.forEach((asset) => {
    let index = parseInt(asset.assetId.substring(0, 2));
    if (index < min) min = index;
    if (index > max) max = index;
  });

  const { createCanvas, loadImage } = require("canvas");
  const canvas = createCanvas(500, 500);
  const ctx = canvas.getContext("2d");

  ctx.fillStyle = "#6996ff";
  ctx.fillRect(0, 0, 500, 500);

  const images = [];

  for (let i = min; i <= max; i++) {
    let asset = assets.find((asset) => {
      return asset.assetId.startsWith(i.toString());
    });
    if (asset) {
      images.push(asset);
    }
  }

  for (let i = 0; i < images.length; i++) {
    let image = await loadImage(images[i].assetURL);
    ctx.drawImage(image, 0, 0, 500, 500);
  }

  let random = Math.floor(Math.random() * 9999)
    .toString()
    .padStart(4, "0");

  ctx.fillStyle = "#000000";
  ctx.font = "bold 30px Courier New";
  ctx.textAlign = "right";
  ctx.textBaseline = "bottom";
  ctx.lineWidth = 3;
  ctx.strokeStyle = "#ffffff";
  ctx.strokeText("#" + random, 500, 500);
  ctx.fillText("#" + random, 500, 500);

  const buffer = canvas.toBuffer("image/png");
  return buffer;
};

module.exports = imageGenerator;
