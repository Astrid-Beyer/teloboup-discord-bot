const { REST, Routes } = require("discord.js");
const getAllFiles = require("./utils/getAllFiles");
const { clientId, guildId, token } = require("./token.json");

const commands = [];
// commandes custom du dossier commands
getAllFiles("./commands").forEach((filePath) =>
  commands.push(require(filePath).data.toJSON())
);

const rest = new REST({ version: "10" }).setToken(token); // instance module REST

(async () => {
  // déploie les commandes
  try {
    console.log(
      `Started refreshing ${commands.length} application (/) commands.`
    );
    const data = await rest.put(
      Routes.applicationGuildCommands(clientId, guildId),
      { body: commands }
    );

    console.log(
      `Successfully reloaded ${data.length} application (/) commands.`
    );
  } catch (error) {
    console.error(error);
  }
})();
