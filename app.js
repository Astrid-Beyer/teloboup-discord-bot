const fs = require("node:fs");
const path = require("node:path");
const getAllFiles = require("./utils/getAllFiles");
const selectMenuTeloru = require("./utils/selectMenuTeloru");
const {
  Client,
  Collection,
  Events,
  GatewayIntentBits,
  AttachmentBuilder,
} = require("discord.js");
const { mongoose, mongo } = require("mongoose");
const { token, mongodb_uri } = require("./token.json");
const imageGenerator = require("./utils/imageGenerator");
const userTelovatar = require("./models/userTelovatar");

const bot = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildPresences,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildEmojisAndStickers,
  ],
});

bot.on("ready", async () => {
  console.log("cache vidé");
  await bot.application.commands.set([]);
});

getAllFiles("./functions").forEach((filePath) => require(filePath)(bot));

bot.commands = new Collection();
bot.selectMenus = new Collection();

getAllFiles("./commands").forEach((filePath) => {
  const command = require(filePath);
  bot.commands.set(command.data.name, command);
});

(async () => {
  try {
    await mongoose.connect(mongodb_uri, { keepAlive: true });
    console.log("Connecté à la base de données.");

    console.log("Connexion au bot...");
    bot.once(Events.ClientReady, (c) => {
      console.log(
        `[${new Date().toString()}] Connecté en tant que ${c.user.tag}`
      );
    });
  } catch (error) {
    console.log(`Error: ${error}`);
  }
})();

bot.on(Events.InteractionCreate, async (interaction) => {
  if (interaction.isChatInputCommand()) {
    const command = interaction.client.commands.get(interaction.commandName);

    if (!command) {
      console.error(
        `No command matching ${interaction.commandName} was found.`
      );
      return;
    }

    try {
      await command.execute(interaction);
    } catch (error) {
      console.error(error);
      if (interaction.replied || interaction.deferred) {
        await interaction.followUp({
          content: "There was an error while executing this command!",
          ephemeral: true,
        });
      } else {
        await interaction.reply({
          content: "There was an error while executing this command!",
          ephemeral: true,
        });
      }
    }
  } else if (interaction.isStringSelectMenu()) {
    if (interaction.customId.startsWith("teloru")) {
      let content = interaction.values[0];
      let row = await selectMenuTeloru(content);

      if (interaction.customId == "teloru4") {
        await interaction.update({ content: "Chargement...", components: [] });
        buffer = await imageGenerator(content);

        const attach = new AttachmentBuilder(buffer, {
          name: interaction.user.id + ".png",
        });

        setTimeout(async () => {
          await interaction.deleteReply();
          await interaction.channel.send({
            files: [attach],
            content: `${interaction.user.toString()} Voici ton Telovatar !`,
            ephemeral: false,
          });
        }, 1000);

        setTimeout(async () => {
          const messages = await interaction.channel.messages.fetch({
            limit: 5,
          });

          const message = messages.find((m) =>
            m.content.includes(interaction.user.id)
          );

          const url = message.attachments.first().url;
          if (!url.endsWith(interaction.user.id + ".png"))
            return interaction.reply({
              content: "Erreur, veuillez réessayer.",
            });
          const userId = interaction.user.id;
          const userTelovatars = await userTelovatar.find({ userId: userId });
          if (userTelovatars.length > 0) {
            await userTelovatar.updateOne(
              { userId: userId },
              { $set: { telovatarURL: url } }
            );
          } else {
            const userTelovatarEntity = new userTelovatar({
              userId: userId,
              telovatarURL: url,
            });
            await userTelovatarEntity.save();
          }
        }, 5000);
      } else {
        await interaction.update({
          components: [row],
        });
      }
    }

    const { selectMenus } = bot;
    const { customId } = interaction;
    const menu = selectMenus.get(customId);
    if (!menu) return new Error("There is no code for this select menu.");

    try {
      await menu.execute(interaction, bot);
    } catch (error) {
      console.error(error);
    }
  }
});

bot.handleComponents();
bot.login(token);
