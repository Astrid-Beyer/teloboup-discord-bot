const { Schema, model } = require("mongoose");

const lvlSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  guildId: {
    type: String,
    required: true,
  },
  xp: {
    type: Number,
    default: 0,
  },
  level: {
    type: Number,
    default: 1,
  },
  hasLeaved: {
    type: Boolean,
    default: false,
  },
});

module.exports = model("Level", lvlSchema);