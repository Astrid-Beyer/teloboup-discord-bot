const { Schema, model } = require("mongoose");

const userAssetSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  assetId: {
    type: String,
    required: true,
  },
});

module.exports = model("UserAsset", userAssetSchema);
