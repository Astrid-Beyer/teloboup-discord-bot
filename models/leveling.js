const { Schema, model } = require("mongoose");

const levelingSchema = new Schema({
  guildId: {
    type: String,
    required: true,
  },
  level: {
    type: Number,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
});

module.exports = model("leveling", levelingSchema);
