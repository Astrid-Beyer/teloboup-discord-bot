const { Schema, model } = require("mongoose");

const currencySchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  guildId: {
    type: String,
    required: true,
  },
  balance: {
    type: Number,
    default: 0,
  },
});

module.exports = model("UserCurrency", currencySchema);
