const { Schema, model } = require("mongoose");

const userTelovatarSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  telovatarURL: {
    type: String,
    required: true,
  },
});

module.exports = model("UserTelovatar", userTelovatarSchema);
