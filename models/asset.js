const { Schema, model } = require("mongoose");

const assetSchema = new Schema({
  assetId: {
    type: String,
    required: true,
  },
  assetName: {
    type: String,
    required: true,
  },
  assetURL: {
    type: String,
    required: true,
  },
  assetLayer: {
    type: Number,
    required: true,
  },
  assetRarity: {
    type: Number,
    required: true,
  },
});

module.exports = model("Asset", assetSchema);
