# TeloBoup

## Un bot Discord JS

Ce bot est à usage personnel pour mon Discord communautaire. Il n'est pas prévu de le rendre modulable pour un autre serveur.

✨ **new!** Giveaway  
✨ **new!** Système monétaire  
Embed modifiable pour les annonces de stream  
Système de leveling, ranking et autoroles

## Commandes réservées aux modérateurs

### /stream

Cette commande permet de créer un embed pour annoncer un stream sur le Discord. Réservée aux modérateurs.

### /clean-stream-idea

Autorise les modérateurs à vider la liste de propositions ou bien un élément de la liste.

### /autorole-configure /autorole-disable

Permet de donner un ou plusieurs rôles quand quelqu'un arrive sur le serveur; Retire un rôle des ajouts automatiques quand quelqu'un débarque sur le serveur.

### /add-leveling-role /remove-leveling-role

Pour donner un rôle spécifique lorsqu'un membre obtient un niveau; Ne propose plus le rôle obtenu à un certain niveau.

### /set-xp /add-XP /remove-XP || /set-level /add-level /remove-level

Permet de gérer les points d'expérience d'un membre du serveur; Permet de gérer les niveaux d'un membre du serveur.

### /set-bal /add-bal /remove-bal

Permet de gérer le solde d'un membre du serveur.

## Commandes disponibles pour tous

### /ping

Premier test de commande, simple ping pour tester le bot.

### /stream-idea

Commande pour proposer une idée de stream sur la chaîne, fonctionne comme une boîte à idées. Sans argument pour afficher la liste, avec un nom de jeu en argument pour faire une proposition. La personne qui fait la demande peut l'annuler mais seul l'admin peut la valider.

### /rank

Voir son niveau et son classement sur le serveur, ainsi que ses points d'expériences.

### /checkin

Récupérer de l'XP quand un stream est lancé.

### /levelboard

Pour voir le classement du serveur, avec autant d'entrées qu'il y a de personnes actives. Il est possible de passer à la page suivante et précédente avec des réactions de message.

## Embed modifiable avec un modal pour la commande /stream

Il est possible de modifier son annonce de la manière suivante :

<p float="left" width="32%">
<img src="https://cdn.discordapp.com/attachments/698203739022032906/1135239566442365039/cap_exemple.png" width="300" align="middle">

<img src="https://cdn.discordapp.com/attachments/698203739022032906/1135239566937301073/cap_modif.png" width="300" align="middle">

<img src="https://cdn.discordapp.com/attachments/698203739022032906/1135239567159590942/cap_modifie.png" width="300" align="middle">
</p>

Les informations sont conservées sur les champs pour pouvoir modifier plus facilement.
