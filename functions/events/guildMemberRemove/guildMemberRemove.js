const { Client, GuildMember } = require("discord.js");
const Level = require("../../../models/level");

module.exports = async (client) => {
  client.on("guildMemberRemove", async (member) => {
    try {
       await Level.findOneAndUpdate({userId: member.id, guildId: member.guild?.id}, { hasLeaved: true });
    } catch (error) {
      console.log(
        `Erreur lors de la suppression du membre: ${error}`
      );
    }
  });
};