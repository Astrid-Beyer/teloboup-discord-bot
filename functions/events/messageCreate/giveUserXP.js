const { addXP } = require("../../../utils/xpHelper");

function getRandomXP(min, max) {
  min = Math.ceil(min);
  max = Math.ceil(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const talkedRecently = new Set();
const perUserCooldown = 60000; // en ms

module.exports = async (client) => {
  client.on("messageCreate", async (message) => {
    if (!message.inGuild() || message.author.bot || talkedRecently.has(message.author.id)) return;

    talkedRecently.add(message.author.id);
    setTimeout(() => {
      talkedRecently.delete(message.author.id);
    }, perUserCooldown);

    const xpToGive = getRandomXP(5, 15);

    try {
      await addXP(message.guild, message.author, xpToGive);
    } catch (error) {
      console.log(`Erreur au moment de donner l'XP: ${error}`);
    }
  });
};
