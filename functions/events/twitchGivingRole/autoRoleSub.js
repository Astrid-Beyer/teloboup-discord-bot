const { Client, GuildMember } = require("discord.js");

module.exports = async (client) => {
  client.on("guildMemberUpdate", async (member) => {
    try {
      let guild = member.guild;
      if (!guild) return;

      const autoRole = "777189794124005427"; // twitch role separator
      const subbedRole = "724620459420942406"; // role given by Twitch integration

      let currDate = new Date();
      let dateFormat = `[${currDate.getHours().toString()}:${currDate
        .getMinutes()
        .toString()}]`;

      if (
        member.roles.cache.find((role) => role.id === subbedRole) &&
        !member.roles.cache.find((role) => role.id === autoRole)
      ) {
        member.roles.add(autoRole);
        console.log(
          `${dateFormat} Rôle séparateur Twitch accordé à ${member.displayName}`
        );
      } else if (
        !member.roles.cache.find((role) => role.id === subbedRole) &&
        member.roles.cache.find((role) => role.id === autoRole)
      ) {
        member.roles.remove(autoRole);
        console.log(
          `${dateFormat} Rôle séparateur Twitch retiré à ${member.displayName}`
        );
      }
    } catch (error) {
      console.log(
        `Erreur, n'a pas réussi à donner automatiquement le rôle Twitch: ${error}`
      );
    }
  });
};
