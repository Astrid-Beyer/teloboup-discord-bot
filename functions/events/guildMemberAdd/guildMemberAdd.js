const { Client, GuildMember } = require("discord.js");
const Level = require("../../../models/level");

module.exports = async (client) => {
  client.on("guildMemberAdd", async (member) => {
    try {
       await Level.findOneAndUpdate({userId: member.id, guildId: member.guild?.id}, { hasLeaved: false });
    } catch (error) {
      console.log(
        `Erreur lors de l'ajout du membre: ${error}`
      );
    }
  });
};