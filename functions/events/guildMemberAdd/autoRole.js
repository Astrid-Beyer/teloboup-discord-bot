const { Client, GuildMember } = require("discord.js");
const AutoRole = require("../../../models/autorole");

module.exports = async (client) => {
  client.on("guildMemberAdd", async (member) => {
    try {
      let guild = member.guild;
      if (!guild) return;

      const autoRole = await AutoRole.find({ guildId: guild.id });
      if (autoRole.length == 0) return;

      for (const autoRoles of autoRole) {
        await member.roles.add(autoRoles.roleId);
      }
    } catch (error) {
      console.log(
        `Erreur, n'a pas réussi à donner automatiquement le rôle: ${error}`
      );
    }
  });
};
