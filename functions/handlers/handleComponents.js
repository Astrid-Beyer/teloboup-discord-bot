const fs = require("fs");

module.exports = (client) => {
  client.handleComponents = async () => {
    const componentFolder = fs.readdirSync(`./components`);
    const { selectMenus } = client;

    for (const file of componentFolder) {
      const menu = require(`../../components/${file}`);
      selectMenus.set(menu.data.name, menu);
    }
  };
};
