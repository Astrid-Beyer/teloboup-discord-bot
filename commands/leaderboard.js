const {
  SlashCommandBuilder,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  EmbedBuilder,
} = require("discord.js");
const Level = require("../models/level");

const getLeaderboardEmbed = async (guild, page, numberPerPage = 10) => {
  const rankedUsers = await Level.find({ guildId: guild.id, hasLeaved: { $ne: true } }, "userId", {
    skip: numberPerPage * (page - 1),
    limit: numberPerPage,
  })
    .sort({ level: "desc", xp: "desc" })
    .select("level xp");
  let embedContent = "";
  for (let i = 0; i < rankedUsers.length; i++) {
    const rank = i + 1 + numberPerPage * (page - 1);
    let rankString;
    switch (rank) {
      case 1:
        rankString = "🥇";
        break;
      case 2:
        rankString = "🥈";
        break;
      case 3:
        rankString = "🥉";
        break;
      default:
        rankString = `${rank} ~`;
    }
    const member = await guild.members.fetch(rankedUsers[i].userId);
    embedContent += ` **${rankString}  ${member.displayName}**\n  ♡ ⁺ Niveau ${rankedUsers[i].level}, *${rankedUsers[i].xp} XP*\n`;
  }

  return new EmbedBuilder()
    .setColor(0xf3dbff)
    .setDescription("### Leaderboard \n" + embedContent)
    .setThumbnail(guild.iconURL())
    .setTimestamp()
    .setFooter({
      text: "ʚ Leaderboard ɞ",
      iconURL:
        "https://cdn.discordapp.com/attachments/698203739022032906/1054399312475476028/download20210304163322.png",
    });
};

const getMaxPage = async (guild, numberPerPage = 10) => {
  count = await Level.count({ guildId: guild.id, hasLeaved: { $ne: true } });
  return Math.ceil(count / numberPerPage);
};

const getButtons = (page, maxPage) => {
  const row = new ActionRowBuilder().addComponents(
    new ButtonBuilder()
      .setCustomId("prev")
      .setStyle(ButtonStyle.Secondary)
      .setEmoji("◀️")
      .setDisabled(page == 1),
    new ButtonBuilder()
      .setCustomId("next")
      .setStyle(ButtonStyle.Secondary)
      .setEmoji("▶️")
      .setDisabled(page == maxPage)
  );
  return row;
};

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();

    try {
      let page = 1;
      const maxPage = await getMaxPage(interaction.guild);
      const isSameUser = (newInteraction) =>
        newInteraction.user.id == interaction.user.id;
      const maxTime = 1000 * 60 * 3; // 3 min

      const message = await interaction.editReply({
        ephemeral: true,
        embeds: [await getLeaderboardEmbed(interaction.guild, page)],
        components: [getButtons(page, maxPage)],
      });

      const collector = message.createMessageComponentCollector({
        filter: isSameUser,
        time: maxTime,
      });

      collector.on("collect", async (btn) => {
        try {
          if (!btn) return;
          btn.deferUpdate();
          if (btn.customId != "prev" && btn.customId != "next") return;
          if (btn.customId == "prev" && page > 1) page--;
          if (btn.customId == "next" && page < maxPage) page++;
          interaction.editReply({
            ephemeral: true,
            embeds: [await getLeaderboardEmbed(interaction.guild, page)],
            components: [getButtons(page, maxPage)],
          });
        } catch (error) {
          console.log(error);
          return;
        }
      });
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("leaderboard")
    .setDescription(`Affiche le classement des membres`),
};
