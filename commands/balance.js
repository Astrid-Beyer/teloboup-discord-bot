const {
  Client,
  Interaction,
  SlashCommandBuilder,
  PermissionFlagsBits,
} = require("discord.js");

const {
  getSavedOrNewUserCurrency,
  getBalanceEmoji,
} = require("../utils/currencyHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }

    const mentionedUser = interaction.options.getUser("membre");
    const targetUserId = mentionedUser || interaction.member;
    const targetUserObj = await interaction.guild.members.fetch(targetUserId);

    const fetchedCurrency = await getSavedOrNewUserCurrency(
      interaction.guild,
      targetUserId
    );
    await interaction.reply(
      `${targetUserObj.user.displayName} possède actuellement ${fetchedCurrency.balance} ${getBalanceEmoji}`
    );
  },
  data: new SlashCommandBuilder()
    .setName("balance")
    .setDescription("Permet de savoir combien de crédits tu possèdes")
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers)
    .addUserOption((option) =>
      option
        .setName("membre")
        .setDescription("Tu peux choisir l'utilisateur qui t'intéresse.")
    ),
};
