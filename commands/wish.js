const {
  ActionRowBuilder,
  SlashCommandBuilder,
  PermissionFlagsBits,
  ButtonStyle,
  ButtonBuilder,
  AttachmentBuilder,
} = require("discord.js");
const {
  getSavedOrNewUserCurrency,
  getBalanceEmoji,
  removeFromBal,
} = require("../utils/currencyHelper");

const Asset = require("../models/asset");

const userAsset = require("../models/userAsset");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("gacha")
    .setDescription("Tente ta chance pour obtenir un objet spécial!"),
  async execute(interaction) {
    const assets = await Asset.find({});
    const userAssets = await userAsset.find({
      userId: interaction.user.id,
    });
    if (userAssets.length == assets.length) {
      await interaction.reply({
        content: `Tu possèdes déjà tous les objets du gacha!`,
        components: [],
      });
      return;
    }

    const fetchedCurrency = await getSavedOrNewUserCurrency(
      interaction.guild,
      interaction.member
    );

    const wishPrice = 1;

    const message = await interaction.reply({
      content: `Tu possèdes actuellement ${fetchedCurrency.balance} ${getBalanceEmoji}. Veux-tu consommer ${wishPrice}x ${getBalanceEmoji} pour tenter ta chance sur le gacha ?`,
      components: [
        new ActionRowBuilder().addComponents(
          new ButtonBuilder()
            .setCustomId("ok")
            .setStyle(ButtonStyle.Success)
            .setLabel("Jouer")
            .setEmoji(getBalanceEmoji),
          new ButtonBuilder()
            .setCustomId("cancel")
            .setStyle(ButtonStyle.Danger)
            .setLabel("Annuler")
        ),
      ],
    });

    const maxTime = 1000 * 60 * 3; // 3min
    const collector = message.createMessageComponentCollector({
      time: maxTime,
      filter: (i) => i.user.id === interaction.user.id,
    });

    collector.on("collect", async (btn) => {
      try {
        if (!btn) return;
        if (btn.customId != "cancel" && btn.customId != "ok") return;
        if (btn.customId == "cancel") {
          collector.stop();
          await btn.update({
            content: `Tirage annulé <:sadd:1145664067122516068>`,
            embeds: [],
            components: [],
          });
        }
        if (btn.customId == "ok") {
          collector.stop();
          if (fetchedCurrency.balance - wishPrice < 0) {
            await btn.update({
              content: `Tu n'as pas assez de ${getBalanceEmoji} pour jouer. Farm moi ça et reviens plus tard!`,
              embeds: [],
              components: [],
            });
          } else {
            removeFromBal(interaction.guild, interaction.member, wishPrice);
            await btn.update({
              content: `Tirage en cours... <a:rolliing:1145657001305718835>`,
              embeds: [],
              components: [],
            });

            const random = Math.random();
            if (random < 0.625) {
              const image = new AttachmentBuilder(
                "https://media.tenor.com/-0gPdn6GMVAAAAAC/genshin3star-wish.gif",
                "common"
              );

              let message = await interaction.followUp({ files: [image] });

              setTimeout(() => message.delete(), 5000);

              const assetsRarity1 = assets.filter(
                (asset) => asset.assetRarity == 1
              );

              const randomAsset =
                assetsRarity1[Math.floor(Math.random() * assetsRarity1.length)];

              const userAssetEntity = new userAsset({
                userId: interaction.user.id,
                assetId: randomAsset.assetId,
              });
              const imageWon = new AttachmentBuilder(
                randomAsset.assetURL,
                randomAsset.assetName
              );

              if (
                userAssets.find(
                  (userAsset) => userAsset.assetId == randomAsset.assetId
                )
              ) {
                setTimeout(
                  async () =>
                    await interaction.followUp({
                      files: [imageWon],
                      content: `${interaction.user.toString()} Tu as eu un doublon! (${
                        randomAsset.assetName
                      })`,
                    }),
                  5000
                );
              } else {
                await userAssetEntity.save();

                setTimeout(
                  async () =>
                    await interaction.followUp({
                      files: [imageWon],
                      content: `${interaction.user.toString()} Tu as obtenu un objet commun! (${
                        randomAsset.assetName
                      })`,
                    }),
                  5000
                );
              }
            } else if (random < 0.875) {
              const image = new AttachmentBuilder(
                "https://media.tenor.com/8OqlJIRATS0AAAAC/wishing-genshin.gif",
                "epic"
              );

              let message = await interaction.followUp({ files: [image] });

              setTimeout(() => message.delete(), 5000);

              const assetsRarity2 = assets.filter(
                (asset) => asset.assetRarity == 2
              );

              const randomAsset =
                assetsRarity2[Math.floor(Math.random() * assetsRarity2.length)];

              const userAssetEntity = new userAsset({
                userId: interaction.user.id,
                assetId: randomAsset.assetId,
              });

              const imageWon = new AttachmentBuilder(
                randomAsset.assetURL,
                randomAsset.assetName
              );

              if (
                userAssets.find(
                  (userAsset) => userAsset.assetId == randomAsset.assetId
                )
              ) {
                setTimeout(
                  async () =>
                    await interaction.followUp({
                      files: [imageWon],
                      content: `${interaction.user.toString()} Tu as eu un doublon! (${
                        randomAsset.assetName
                      })`,
                    }),
                  5000
                );
              } else {
                await userAssetEntity.save();

                setTimeout(
                  async () =>
                    await interaction.followUp({
                      files: [imageWon],
                      content: `${interaction.user.toString()} Tu as obtenu un objet épique !! (${
                        randomAsset.assetName
                      })`,
                    }),
                  5000
                );
              }
            } else {
              const image = new AttachmentBuilder(
                "https://media.tenor.com/Z5kE_QEkGdgAAAAd/roll-wishing.gif",
                "legendary"
              );

              let message = await interaction.followUp({ files: [image] });

              setTimeout(() => message.delete(), 5000);

              const assetsRarity3 = assets.filter(
                (asset) => asset.assetRarity == 3
              );

              const randomAsset =
                assetsRarity3[Math.floor(Math.random() * assetsRarity3.length)];

              const userAssetEntity = new userAsset({
                userId: interaction.user.id,
                assetId: randomAsset.assetId,
              });
              await userAssetEntity.save();
              const imageWon = new AttachmentBuilder(
                randomAsset.assetURL,
                randomAsset.assetName
              );

              if (
                userAssets.find(
                  (userAsset) => userAsset.assetId == randomAsset.assetId
                )
              ) {
                setTimeout(
                  async () =>
                    await interaction.followUp({
                      files: [imageWon],
                      content: `${interaction.user.toString()} Tu as eu un doublon! (${
                        randomAsset.assetName
                      })`,
                    }),
                  5000
                );
              } else {
                await userAssetEntity.save();

                setTimeout(
                  async () =>
                    await interaction.followUp({
                      files: [imageWon],
                      content: `${interaction.user.toString()} Tu as obtenu un objet légendaire !!! (${
                        randomAsset.assetName
                      })`,
                    }),
                  5000
                );
              }
            }
          }
        }
      } catch (e) {
        console.error(e);
      }
    });
  },
};
