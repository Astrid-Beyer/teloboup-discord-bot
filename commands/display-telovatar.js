const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const Telovatar = require("../models/userTelovatar");

module.exports = {
  async execute(interaction) {
    const fetchedAvatar = await Telovatar.findOne({
      userId: interaction.user.id,
    });
    if (!fetchedAvatar) {
      interaction.reply(
        "Tu n'as pas encore créé ton avatar. Utilise `/teloru` pour en créer un!"
      );
      return;
    }

    await interaction.reply(fetchedAvatar.telovatarURL);
  },
  data: new SlashCommandBuilder()
    .setName("display-telovatar")
    .setDescription("Te renvoi ton telovatar actuel"),
};
