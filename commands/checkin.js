const { Client, Interaction, SlashCommandBuilder } = require("discord.js");
const { getSavedOrNewUserLevel, addXP } = require("../utils/xpHelper");
const calculateLvlXP = require("../utils/calculateLvlXP");
const Level = require("../models/level");

const checkedIn = new Set();
const perUserCooldown = 18000000; // en ms (5H)

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    if (checkedIn.has(interaction.user.id)) {
      interaction.reply(
        "Hep hep! T'as déjà récupéré le billet toi!! Je suis capable de te retirer de l'XP 'tention è_é"
      );
      return;
    }

    const level = await getSavedOrNewUserLevel(
      interaction.guild,
      interaction.user
    );

    checkedIn.add(interaction.user.id);
    setTimeout(() => {
      checkedIn.delete(interaction.user.id);
    }, perUserCooldown);

    await interaction.deferReply();

    const requiredXP = calculateLvlXP(level.level);

    const amount = Math.ceil(requiredXP / 4);

    try {
      await addXP(interaction.guild, interaction.user, amount);
      interaction.followUp("Tu reçois de l'XP, profite bien du live <3");
    } catch (error) {
      console.log(error);
    }
  },
  data: new SlashCommandBuilder()
    .setName("checkin")
    .setDescription(`🎟️ Prends ton billet pour le live de Teloru!`),
};
