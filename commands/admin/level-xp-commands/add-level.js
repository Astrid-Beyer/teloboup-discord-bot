const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const { addLevel } = require("../../../utils/xpHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("nombre");
    try {
      await addLevel(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("add-level")
    .setDescription(`Ajoute un niveau à un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("L'utilisateur pour qui il faut ajouter des niveaux.")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option
        .setName("nombre")
        .setDescription("Nombre de niveau à ajouter. Par défaut 1.")
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
