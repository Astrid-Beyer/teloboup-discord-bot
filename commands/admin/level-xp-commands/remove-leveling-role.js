const {
  SlashCommandBuilder,
  Client,
  Interaction,
  PermissionFlagsBits,
} = require("discord.js");
const Leveling = require("../../../models/leveling");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commmande n'est disponible que dans un serveur."
      );
      return;
    }

    const targetLevelValue = interaction.options.get("niveau").value;

    try {
      await interaction.deferReply();

      await Leveling.deleteOne({
        guildId: interaction.guild.id,
        level: targetLevelValue,
      });

      interaction.editReply(
        `Le rôle automatique a été désactivé pour le niveau ${targetLevelValue}.`
      );
    } catch (error) {
      console.log(error);
    }
  },
  data: new SlashCommandBuilder()
    .setName("remove-leveling-role")
    .setDescription("configuration des rôles automatiques du serveur.")
    .addStringOption((option) =>
      option
        .setName("niveau")
        .setDescription("Quel niveau ne doit plus donner de rôle ?")
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
