const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const { addXP } = require("../../../utils/xpHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("xp");
    try {
      await addXP(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("add-xp")
    .setDescription(`Offre des point d'expérience à un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("Qui va recevoir de l'XP ?")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option.setName("xp").setDescription("Combien de points donner ?")
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
