const {
  SlashCommandBuilder,
  Client,
  Interaction,
  PermissionFlagsBits,
} = require("discord.js");
const Leveling = require("../../../models/leveling");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commmande n'est disponible que dans un serveur."
      );
      return;
    }

    const targetRoleId = interaction.options.get("role").value;
    const targetLevelValue = interaction.options.get("niveau").value;

    try {
      await interaction.deferReply();

      await Leveling.findOneAndUpdate(
        { guildId: interaction.guild.id, level: targetLevelValue },
        { role: targetRoleId },
        { upsert: true }
      );

      interaction.editReply(
        `Le rôle automatique a été configuré pour le niveau ${targetLevelValue}.`
      );
    } catch (error) {
      console.log(error);
    }
  },
  data: new SlashCommandBuilder()
    .setName("add-leveling-role")
    .setDescription("configuration des rôles automatiques du serveur.")
    .addRoleOption((option) =>
      option
        .setName("role")
        .setDescription(
          "Le rôle que doit prendre l'utilisateur quand il obtient le niveau renseigné."
        )
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("niveau")
        .setDescription(
          "A quel niveau l'utilisateur va prendre un nouveau rôle ?"
        )
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
