const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const { removeXP } = require("../../../utils/xpHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("xp");
    try {
      await removeXP(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("remove-xp")
    .setDescription(`Retire des point d'expérience à un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("Qui va perdre de l'XP ?")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option.setName("xp").setDescription("Combien de points enlever ?")
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
