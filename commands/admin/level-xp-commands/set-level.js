const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const { setLevel } = require("../../../utils/xpHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("level");
    try {
      await setLevel(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("set-level")
    .setDescription(`Fixe un niveau à un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("Membre pour lequel le niveau sera modifiée")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option.setName("level").setDescription("Niveau à fixer")
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
