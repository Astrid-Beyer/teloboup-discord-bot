const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const { setXP } = require("../../../utils/xpHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("xp");
    try {
      await setXP(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("set-xp")
    .setDescription(`Fixe de l'XP à un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("Membre pour lequel l'XP sera modifiée")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option.setName("xp").setDescription("Nombre de points à fixer")
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
