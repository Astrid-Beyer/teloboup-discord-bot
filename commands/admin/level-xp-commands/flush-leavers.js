const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const Level = require("../../../models/level");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    try {
        const currentMembers = await interaction.guild.members.fetch();
        const res = await Level.updateMany({ userId: { $nin: currentMembers.map(member => member.id) }, guildId: interaction.guild.id, hasLeaved: { $ne: true } }, { hasLeaved: true });
        interaction.reply(`Finito, ${res.modifiedCount} utilisateur(s) mis à jour.`);
      } catch (error) {
        console.log(error);
      }
  },

  data: new SlashCommandBuilder()
    .setName("flush-leavers")
    .setDescription(`Assigne un status spécial aux utilisateurs en BDD qui ont quittés le serveur.`)
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};