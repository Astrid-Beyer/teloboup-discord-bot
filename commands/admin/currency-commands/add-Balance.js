const {
  Interaction,
  SlashCommandBuilder,
  PermissionFlagsBits,
} = require("discord.js");
const { addToBal } = require("../../../utils/currencyHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("gain");
    try {
      await addToBal(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("add-balance")
    .setDescription(`Alimente le solde d'un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("A qui doit-on modifier le solde ?")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option
        .setName("gain")
        .setDescription("Combien va-t-il recevoir ?")
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
