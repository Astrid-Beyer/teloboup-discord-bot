const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const { setBal } = require("../../../utils/currencyHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("solde");
    try {
      await setBal(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("set-balance")
    .setDescription(`Fixe le solde d'un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("A qui doit-on modifier le solde ?")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option
        .setName("solde")
        .setDescription("A combien fixer son solde ?")
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
