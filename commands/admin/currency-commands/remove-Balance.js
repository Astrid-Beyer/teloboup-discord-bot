const {
  Interaction,
  SlashCommandBuilder,
  PermissionFlagsBits,
} = require("discord.js");
const { removeFromBal } = require("../../../utils/currencyHelper");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }
    await interaction.deferReply();
    const user = interaction.options.getUser("utilisateur");
    const amount = interaction.options.getInteger("perte");
    try {
      await removeFromBal(interaction.guild, user, amount, interaction);
    } catch (error) {
      console.log(error);
    }
  },

  data: new SlashCommandBuilder()
    .setName("remove-balance")
    .setDescription(`Déduit le solde d'un utilisateur`)
    .addUserOption((option) =>
      option
        .setName("utilisateur")
        .setDescription("A qui doit-on modifier le solde ?")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option
        .setName("perte")
        .setDescription("Combien va-t-il perdre ?")
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
