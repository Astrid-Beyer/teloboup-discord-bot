const fs = require("fs");

const {
  SlashCommandBuilder,
  StringSelectMenuBuilder,
  ActionRowBuilder,
  PermissionFlagsBits,
} = require("discord.js");

let ideas = new Map(Object.entries(JSON.parse(fs.readFileSync("ideas.json"))));
let allOptions = [
  {
    label: `Tout supprimer`,
    description: `Vider la liste`,
    value: `*`,
  },
];

for (const [key, value] of ideas) {
  console.log(`${key}: ${value}`);
  allOptions.push({
    label: `${value}`,
    description: `Supprimer ${value} de la liste`,
    value: `${value}`,
  });
}

module.exports = {
  data: new SlashCommandBuilder()
    .setName("clean-stream-idea")
    .setDescription("effacer la liste d'idées")
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
  async execute(interaction, client) {
    const menu = new StringSelectMenuBuilder()
      .setCustomId("clean-stream-idea")
      .setMinValues(1)
      .setMaxValues(1)
      .setOptions(allOptions);
    await interaction.reply({
      components: [new ActionRowBuilder().addComponents(menu)],
    });
  },
};
