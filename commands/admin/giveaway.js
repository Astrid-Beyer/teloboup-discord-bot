const {
  SlashCommandBuilder,
  PermissionFlagsBits,
  EmbedBuilder,
} = require("discord.js");
const { getSavedOrNewUserLevel } = require("../../utils/xpHelper");
const ms = require("ms");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("giveaway")
    .setDescription("setup un giveaway")
    .addStringOption((option) =>
      option
        .setName("lot")
        .setDescription("Que fait-on gagner ?")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("message")
        .setDescription("Personnalise le message de giveaway!")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("emoji")
        .setDescription("Avec quel emoji les participants doivent réagir ?")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("durée")
        .setDescription(
          "Durée du giveaway : '2h' pour 2heures, '2d' pour 2 jours..."
        )
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option
        .setName("rang-requis")
        .setDescription("Le niveau minimum requis pour participer")
        .setMinValue(1)
    )
    .addIntegerOption((option) =>
      option
        .setName("nb-gagnants")
        .setDescription("Nombre de gagnants à tirer. Par défaut 1.")
        .setMinValue(1)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),

  async execute(interaction) {
    let prize = interaction.options.getString("lot");
    let text = interaction.options.getString("message");
    let chosenEmoji = interaction.options.getString("emoji");
    let duration = interaction.options.getString("durée");
    let levelMin = interaction.options.getInteger("rang-requis");
    let nbWinners = interaction.options.getInteger("nb-gagnants") ?? 1;
    let guild = interaction.guild;

    const emojiId = /<(?:a)?:.+:(\d+)>/.exec(chosenEmoji)?.[1];
    const computeDuration = `<t:${Math.round(
      (Date.now() + ms(duration)) / 1000
    )}:R>`;
    const registered = new Set();
    const channelAnnonceId = "724652951435739167"; // channel annonces

    let newEmbed = new EmbedBuilder()
      .setColor(0xf3dbff)
      .setTitle(prize)
      .setDescription(
        `${text}\n\n Fin: ${computeDuration}\n Participations: **${registered.size}**`
      )
      .setFooter({
        text: "ʚ Giveaway ɞ",
        iconURL:
          "https://cdn.discordapp.com/attachments/698203739022032906/1054399312475476028/download20210304163322.png",
      });

    await interaction.deferReply();

    const channelAnnonce = await interaction.guild.channels.fetch(
      channelAnnonceId
    );
    const giveawayRoleId = "1138882151916519507";
    const embedMessage = await channelAnnonce.send(`<@&${giveawayRoleId}>`);
    embedMessage.edit({ embeds: [newEmbed] });
    const collector = embedMessage.createReactionCollector({
      filter: (reaction) => {
        return (
          (reaction.emoji.id && reaction.emoji.id === emojiId) ||
          reaction.emoji.name == chosenEmoji
        );
      },
      time: ms(duration),
      idle: ms(duration),
    });

    collector.on("collect", async (reaction, user) => {
      let canRegister = true;
      if (levelMin != null) {
        const level = await getSavedOrNewUserLevel(guild, user);
        canRegister = level.level >= levelMin;
      }
      if (canRegister) {
        registered.add(user.id);
        newEmbed = new EmbedBuilder()
          .setColor(0xf3dbff)
          .setTitle(prize)
          .setDescription(
            `${text}\n\n Fin: ${computeDuration}\n Participations: **${registered.size}**`
          )
          .setFooter({
            text: "ʚ Giveaway ɞ",
            iconURL:
              "https://cdn.discordapp.com/attachments/698203739022032906/1054399312475476028/download20210304163322.png",
          });
        embedMessage.edit({ embeds: [newEmbed] });
      }
    });

    collector.on("end", async (reactions) => {
      if (registered.size == 0)
        embedMessage.reply(`Aucune participation enregistrée.`);
      else {
        const winners = [];
        do {
          const winnerId = [...registered][
            Math.floor(Math.random() * registered.size)
          ];
          registered.delete(winnerId);
          winners.push(await guild.members.fetch(winnerId));
          nbWinners--;
        } while (nbWinners > 0 && registered.size > 0);

        const winSpeech =
          winners.length > 1
            ? `vous remportez le giveaway! Vous obtenez ${prize}. \nUn modérateur va vous contacter, assurez-vous d'avoir vos messages privés ouverts!`
            : `tu remportes le giveaway! Tu obtiens ${prize}. \nUn modérateur va te contacter, assure-toi d'avoir tes messages privés ouverts!`;

        embedMessage.reply(
          `Félicitations à ${new Intl.ListFormat("fr").format(
            winners.map((winner) => winner.toString())
          )}, ${winSpeech}`
        );
      }
    });

    interaction.editReply(
      `Annonce de giveaway envoyée sur le channel ${channelAnnonce.toString()}`
    );
  },
};
