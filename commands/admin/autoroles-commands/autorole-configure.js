const {
  SlashCommandBuilder,
  Client,
  Interaction,
  PermissionFlagsBits,
} = require("discord.js");
const AutoRole = require("../../../models/autorole");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commmande n'est disponible que dans un serveur."
      );
      return;
    }

    const targetRoleId = interaction.options.get("role").value;

    try {
      await interaction.deferReply();
      let autoRoles = await AutoRole.find({
        guildId: interaction.guild.id,
      });

      if (autoRoles.some((autoRole) => autoRole.roleId === targetRoleId)) {
        interaction.editReply(
          "Le rôle automatique a déjà été configuré pour ce rôle. Pour le désactiver, utiliser `/autorole-disable`."
        );
        return;
      }

      await AutoRole.create({
        guildId: interaction.guild.id,
        roleId: targetRoleId,
      });
      interaction.editReply("Le rôle automatique a été configuré.");
    } catch (error) {}
  },
  data: new SlashCommandBuilder()
    .setName("autorole-configure")
    .setDescription("configuration des rôles automatiques du serveur.")
    .addRoleOption((option) =>
      option
        .setName("role")
        .setDescription(
          "Le rôle que doit prendre l'utilisateur quand il rejoint le serveur."
        )
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
