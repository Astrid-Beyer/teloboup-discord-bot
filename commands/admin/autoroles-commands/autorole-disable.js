const {
  SlashCommandBuilder,
  Client,
  Interaction,
  PermissionFlagsBits,
} = require("discord.js");
const AutoRole = require("../../../models/autorole");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commmande n'est disponible que dans un serveur."
      );
      return;
    }

    const targetRoleId = interaction.options.get("role").value;

    try {
      await interaction.deferReply();
      if (
        !(await AutoRole.exists({
          guildId: interaction.guild.id,
          roleId: targetRoleId,
        }))
      ) {
        interaction.editReply(
          "Le rôle automatique n'a pas été configuré sur le serveur. Utiliser `/autorole-configure` pour le configurer."
        );
        return;
      }

      await AutoRole.findOneAndDelete({ guildId: interaction.guild.id });
      interaction.editReply(
        "Le rôle automatique a été désactivé sur le serveur."
      );
    } catch (error) {
      console.log(error);
    }
  },
  data: new SlashCommandBuilder()
    .setName("autorole-disable")
    .setDescription("désactive des rôles automatiques du serveur.")
    .addRoleOption((option) =>
      option
        .setName("role")
        .setDescription(
          "Le rôle qui ne doit plus être attribué par défaut à l'utilisateur quand il arrive sur le serveur."
        )
        .setRequired(true)
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
};
