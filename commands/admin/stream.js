const {
  SlashCommandBuilder,
  EmbedBuilder,
  PermissionFlagsBits,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
} = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("stream")
    .setDescription("setup une annonce de live")
    .addStringOption((option) =>
      option
        .setName("en-tête")
        .setDescription('texte en gras après "**LIVE** ›"')
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("contenu")
        .setDescription("corps du message d'annonce")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("gif")
        .setDescription("lien vers un gif approprié au stream")
    )
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
  async execute(interaction) {
    const modo = interaction.user;
    let head = interaction.options.getString("en-tête");
    let content = interaction.options.getString("contenu");
    let gif = interaction.options.getString("gif");
    const hype = "<:teloruHYPEE:1083296837773053982>";

    let newEmbed = new EmbedBuilder()
      .setColor(0xf3dbff)
      .setDescription(
        `\n🍡\n╭・**LIVE!** › \`${head}\`\n・*${content}* \n╰・ [twitch.tv/teloru](https://www.twitch.tv/teloru)`
      )
      .setImage(gif)
      .setThumbnail(
        "https://media.discordapp.net/attachments/777227909588713522/1083141113126334545/dormir-crop_crop.png?width=672&height=671"
      )
      .setFooter({
        text: "Merci " + modo.username + " pour l'annonce <3",
        iconURL: modo.avatarURL(),
      });

    const message = await interaction.reply({
      content: "Aperçu de l'annonce :",
      embeds: [newEmbed],
      components: [
        new ActionRowBuilder().addComponents(
          new ButtonBuilder()
            .setCustomId("cancel")
            .setStyle(ButtonStyle.Danger)
            .setLabel("Annuler"),
          new ButtonBuilder()
            .setCustomId("edit")
            .setStyle(ButtonStyle.Secondary)
            .setLabel("Éditer"),
          new ButtonBuilder()
            .setCustomId("ok")
            .setStyle(ButtonStyle.Success)
            .setLabel("Valider l'annonce")
        ),
      ],
    });

    const maxTime = 1000 * 60 * 3; // 3min
    const collector = message.createMessageComponentCollector({
      time: maxTime,
    });

    collector.on("collect", async (btn) => {
      try {
        if (!btn) return;
        if (
          btn.customId != "cancel" &&
          btn.customId != "ok" &&
          btn.customId != "edit"
        )
          return;
        if (btn.customId == "cancel") {
          collector.stop();
          await btn.update({
            content: `Annonce annulée par ${btn.user}.`,
            embeds: [],
            components: [],
          });
        }
        if (btn.customId == "ok") {
          collector.stop();
          const channelAnnonceId = "724652951435739167";
          const channelAnnonce =
            interaction.guild.channels.cache.get(channelAnnonceId);
          channelAnnonce.send("@everyone " + hype);
          channelAnnonce.send({ embeds: [newEmbed] });
          await btn.update({
            content: `Annonce validée par ${btn.user}.`,
            embeds: [],
            components: [],
          });

          let channelCheckInId = "1131603819504095434"; // channel ckeckin
          let everyoneID = "724619845177442395";
          let timeOpen = 10800000;

          const channelCheckIn = await interaction.guild.channels.fetch(
            channelCheckInId
          );

          const role = await interaction.guild.roles.fetch(everyoneID);

          channelCheckIn.permissionOverwrites.edit(role, {
            SendMessages: true,
            ViewChannel: true,
          });

          setTimeout(() => {
            channelCheckIn.permissionOverwrites.edit(role, {
              SendMessages: false,
              ViewChannel: false,
            });
          }, timeOpen);
        }
        if (btn.customId == "edit") {
          const modal = new ModalBuilder()
            .setCustomId("edition")
            .setTitle("Édition");

          const headerInput = new TextInputBuilder()
            .setCustomId("headerInput")
            .setLabel("En-tête")
            .setStyle(TextInputStyle.Short)
            .setValue(head)
            .setRequired(true);
          const contentInput = new TextInputBuilder()
            .setCustomId("contentInput")
            .setLabel("Contenu")
            .setStyle(TextInputStyle.Paragraph)
            .setValue(content)
            .setRequired(true);
          const gifInput = new TextInputBuilder()
            .setCustomId("gifInput")
            .setLabel("Gif")
            .setStyle(TextInputStyle.Short)
            .setPlaceholder("Lien vers un gif approprié au stream.")
            .setValue(gif ?? "")
            .setRequired(false);

          modal.addComponents(
            new ActionRowBuilder().addComponents(headerInput),
            new ActionRowBuilder().addComponents(contentInput),
            new ActionRowBuilder().addComponents(gifInput)
          );

          await btn.showModal(modal);

          const submitted = await btn
            .awaitModalSubmit({
              time: 120000, // 2 min
              filter: (i) => i.user.id === btn.user.id,
            })
            .catch((err) => {
              console.log(`je crash ici :\n${err}`);
            });

          if (!submitted) return;
          head = submitted.fields.getTextInputValue("headerInput");
          content = submitted.fields.getTextInputValue("contentInput");
          const newGif = submitted.fields.getTextInputValue("gifInput");
          if (
            newGif.match(
              /(?:([^:\/?#]+):)?(?:\/\/([^/?#]*))?([^?#]*\.(?:gif))(?:\?([^#]*))?(?:#(.*))?/g
            )
          )
            gif = newGif;
          else if (newGif == "") gif = null;
          else gif = gif || null;
          newEmbed = new EmbedBuilder()
            .setColor(0xf3dbff)
            .setDescription(
              `\n🍡\n╭・**LIVE!** › \`${head}\`\n・*${content}* \n╰・ [twitch.tv/teloru](https://www.twitch.tv/teloru)`
            )
            .setImage(gif)
            .setThumbnail(
              "https://media.discordapp.net/attachments/777227909588713522/1083141113126334545/dormir-crop_crop.png?width=672&height=671"
            )
            .setFooter({
              text: "Merci " + modo.username + " pour l'annonce <3",
              iconURL: modo.avatarURL(),
            });
          await submitted.update({ embeds: [newEmbed] });
        }
      } catch (error) {
        console.log(error);
        return;
      }
    });
  },
};
