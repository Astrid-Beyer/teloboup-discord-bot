const {
  SlashCommandBuilder,
  PermissionFlagsBits,
  StringSelectMenuOptionBuilder,
  StringSelectMenuBuilder,
  ActionRowBuilder,
} = require("discord.js");

const Asset = require("../models/asset");

const userAsset = require("../models/userAsset");

module.exports = {
  data: new SlashCommandBuilder().setName("teloru").setDescription("Secret"),
  async execute(interaction) {
    const userAssets = await userAsset.find({ userId: interaction.user.id });

    console.log(interaction.user.id);

    min = -9;

    const assets = await Asset.find({
      assetId: { $regex: "^" + min },
    });

    let = placeholder = "Choisis un fond";

    let StringSelectMenuArray = [];

    assets.forEach((asset) => {
      if (
        userAssets.find((userAsset) => userAsset.assetId == asset.assetId) ||
        asset.assetRarity == 0
      ) {
        StringSelectMenuArray.push(
          new StringSelectMenuOptionBuilder()
            .setLabel(asset.assetName)
            .setValue(interaction.user.id + ":" + asset.assetId)
        );
      }
    });

    const selectMenu = new StringSelectMenuBuilder()
      .setCustomId("teloru" + min.toString())
      .setPlaceholder(placeholder)
      .addOptions(
        StringSelectMenuArray.map((StringSelectMenuOptionBuilder) => {
          return StringSelectMenuOptionBuilder;
        })
      );

    selectMenu.addOptions(
      new StringSelectMenuOptionBuilder()
        .setLabel("Aucun")
        .setValue(interaction.user.id + ":" + min + "_NaN")
    );

    const row1 = new ActionRowBuilder().addComponents(selectMenu);

    const username = interaction.user.username;

    await interaction.reply({
      ephemeral: true,
      content: "Telovatar de " + username,
      components: [row1],
    });
  },
};
