const {
  Client,
  Interaction,
  SlashCommandBuilder,
  AttachmentBuilder,
} = require("discord.js");
const canvacord = require("canvacord");
const calculateLvlXP = require("../utils/calculateLvlXP");

const Level = require("../models/level");

module.exports = {
  execute: async (interaction) => {
    if (!interaction.inGuild()) {
      interaction.reply(
        "Cette commande ne peut qu'être exécutée sur un serveur."
      );
      return;
    }

    await interaction.deferReply();
    const mentionedUserId = interaction.options.get("membre")?.value;
    const targetUserId = mentionedUserId || interaction.member.id;
    const targetUserObj = await interaction.guild.members.fetch(targetUserId);

    const fetchedLevel = await Level.findOne({
      userId: targetUserId,
      guildId: interaction.guild.id,
    });

    if (!fetchedLevel) {
      interaction.editReply(
        mentionedUserId
          ? `${targetUserObj.user.username} n'a pas encore de niveau.`
          : ``
      );
      return;
    }

    let allLevels = await Level.find({
      guildId: interaction.guild.id, hasLeaved: { $ne: true }
    }).select("-_id userId level xp");

    allLevels.sort((a, b) => {
      if (a.level === b.level) {
        return b.xp - a.xp;
      } // si on est sur le même lvl, on compare les xp
      else {
        return b.level - a.level;
      }
    });
    let currentRank =
      allLevels.findIndex((lvl) => lvl.userId === targetUserId) + 1;

    const rank = new canvacord.Rank()
      .setAvatar(targetUserObj.user.displayAvatarURL({ size: 256 }))
      .setRank(currentRank)
      .setLevel(fetchedLevel.level)
      .setCurrentXP(fetchedLevel.xp)
      .setRequiredXP(calculateLvlXP(fetchedLevel.level))
      .setProgressBar("#f3dbff", "COLOR")
      .setUsername(targetUserObj.user.username)
      .setBackground(
        "IMAGE",
        "https://cdn.discordapp.com/attachments/804966337604354058/1131012328964493392/eb954b91385696f3f4dd952ce2bb268b.jpg"
      )
      // .setStatus(targetUserObj.presence.status)
      .setFontSize(12);

    const data = await rank.build();
    const attachment = new AttachmentBuilder(data);
    interaction.editReply({ files: [attachment] });
  },

  data: new SlashCommandBuilder()
    .setName("rank")
    .setDescription(`Voir ta progression d'XP sur le serveur`)
    .addUserOption((option) =>
      option
        .setName("membre")
        .setDescription("Tu peux choisir l'utilisateur qui t'intéresse.")
    ),
};
