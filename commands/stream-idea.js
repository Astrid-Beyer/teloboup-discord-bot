const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const { twitch_clientId, auth_token } = require('../token.json');
const axios = require('axios');
const fs = require('fs');

let cooldown = [];

module.exports = {
    data: new SlashCommandBuilder()
        .setName('stream-idea')
        .setDescription('Voir les idées de stream pour Teloru, tu peux ajouter un jeu à la liste!')
        .addStringOption(option =>
            option
            .setName('jeu')
            .setDescription('Ajouter un jeu')),
    async execute(interaction) {
        const color = 0xF3DBFF;
        const title = 'Propositions de streams';
        let description = 'Voici la liste d\'idées de jeux à stream pour la chaîne de @Teloru.\n\n **⊱━━━━━━━━━━━━ « ⋅ʚ♡ɞ⋅ » ━━━━━━━━━━━━━⊰**';

        const userId        = '111892702152384512'; // Teloru ID
        const suggesterId   = interaction.member.id; // id de la personne qui lance la commande
        const agreeEmoji    = '✅';
        const disagreeEmoji = '❌';

        let ideas = new Map(Object.entries(JSON.parse(fs.readFileSync('ideas.json'))));
        const newIdea = interaction.options.getString('jeu');

        if (interaction.options.getString('jeu')) { // si un jeu a été proposé
            if (cooldown.includes(suggesterId)) return await interaction.reply({content: `Tu as déjà fait une proposition récemment, reviens dans 2 minutes.`});
            
            coolingDown(suggesterId, userId);
            
            axios.post('https://api.igdb.com/v4/games', `fields: name; search: "${newIdea}"; limit: 1;`,
            {
                headers: {
                    "Client-ID": twitch_clientId,
                    Authorization: "Bearer " + auth_token
                }
            })
            .then(async function (response) {
                try {
                    let game = response.data[0].name;
                    let idGame = response.data[0].id;

                    if (ideas.has(`${idGame}`)) await interaction.reply(`"${game}" a déjà été proposé.`);

                    else {
                        const confirmMessage = await interaction.reply({ content:`Le jeu "${game}" a été proposé. L'ajouter à la liste ? Seule @Teloru peut valider la proposition.`, fetchReply: true });

                        confirmMessage.react(agreeEmoji);
                        confirmMessage.react(disagreeEmoji);
    
                        const filter = (reaction, user) => {
                            return ((reaction.emoji.name === agreeEmoji || reaction.emoji.name === disagreeEmoji) && user.id === userId) || (reaction.emoji.name === disagreeEmoji && user.id === suggesterId);
                        };
                    
                        confirmMessage.awaitReactions( {filter, max: 1, errors: ['time'] })
                        .then(collected => {
                            const reaction = collected.first();
    
                            if (reaction.emoji.name === agreeEmoji) {
                                ideas.set(`${idGame}`, game);
                                interaction.editReply(`"${game}" a été ajouté à la liste.`);
                                fs.writeFileSync('ideas.json', JSON.stringify(Object.fromEntries(ideas)));
                                    
                            } else {
                                interaction.editReply('La proposition a été rejetée.');
                            }
                        })
                    }
                } catch (error) {
                    console.log(error);
                    await interaction.reply({ content:`Je ne trouve pas le jeu "${newIdea}". L'as-tu bien orthographié ?`});
                }
            })
            .catch(async function (error) {
                console.log(error);
                await interaction.editReply(`Ce jeu est introuvable.`);
            });
        } else {
            if (ideas.size == 0) description += "\n\n⠀⠀Aucun jeu n'a encore été proposé !";
            else {
                for (let[key, value] of ideas) {
                    description += `\n⠀⨯⠀ ${value}`;
                }
            }
            const editedEmbed = new EmbedBuilder()
            .setTitle(title)
            .setColor(color)
            .setDescription(description);

            await interaction.reply({embeds: [editedEmbed] });
        }
    }
}

function coolingDown(suggesterId, userId) {
    if (suggesterId != userId) {
        cooldown.push(suggesterId);
        setTimeout(() => {
            cooldown.shift();
        }, 20000)
    }
}