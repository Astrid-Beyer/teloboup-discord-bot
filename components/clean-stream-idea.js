const { SlashCommandBuilder, PermissionFlagsBits } = require("discord.js");
const fs = require("fs");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("clean-stream-idea")
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
  async execute(interaction, client) {
    const ideasData = fs.readFileSync("ideas.json", "utf8");
    let ideas = JSON.parse(ideasData);
    const toRemove = interaction.values[0];

    if (toRemove === "*") {
      ideas = {}; // Supprimer tous les éléments du JSON en remplaçant par un objet vide
      fs.writeFileSync("ideas.json", "{}"); // Écrire un objet JSON vide dans le fichier
      await interaction.reply({
        content: "Toutes les idées ont été supprimées.",
      });
    } else {
      const keyToRemove = Object.keys(ideas).find(
        (key) => ideas[key] === toRemove
      );
      if (keyToRemove) {
        delete ideas[keyToRemove];
        const replaced = JSON.stringify(ideas);
        fs.writeFileSync("ideas.json", replaced);
        await interaction.reply({
          content: `${toRemove} a bien été supprimé.`,
        });
      } else {
        await interaction.reply({
          content: `Impossible de trouver ${toRemove} dans la liste des idées.`,
        });
      }
    }
  },
};
